package com.example.demo.repository;

import com.example.demo.model.Selection;
import org.springframework.dao.DuplicateKeyException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
class SelectionRepositoryTest {
  @Autowired
  private SelectionRepository selectionRepository;
  @AfterEach
  void drop(){
    selectionRepository.deleteAll();
  }

  @Test
  void shouldRenderSelectionWhenSavingSelection(){
    Selection selection = new Selection();
    selection.setName("test_selection");
    assertEquals(selectionRepository.save(selection),selection);
  }
  @Test
  void shouldCreateAnIdWhenSavingSelection(){
    Selection selection = new Selection();
    selection.setName("test_selection_id");
    assertNotNull(selectionRepository.save(selection).getId());
  }
  @Test
  void shouldRenderErrorIfNameAndUUIDAreDuplicate(){
    Selection selection = new Selection();
    selection.setName("test_selection_dup");
    selection.setUuid("01");
    Selection selection2 = new Selection();
    selection2.setName("test_selection_dup");
    selection2.setUuid("01");
    selectionRepository.save(selection);
    assertThrows(DuplicateKeyException.class,()->selectionRepository.save(selection2));
  }
  @Test
  void shouldSuccessIfNameIsDuplicateAndUUIDDifferent(){
    Selection selection = new Selection();
    selection.setName("test_selection_dup");
    selection.setUuid("01");
    Selection selection2 = new Selection();
    selection2.setName("test_selection_dup");
    selection2.setUuid("02");
    selectionRepository.save(selection);
    assertDoesNotThrow(()->selectionRepository.save(selection2));
  }
  @Test
  void shouldFindAllByUUID(){
    Selection selection = new Selection();
    selection.setName("selection_0");
    selection.setUuid("01");
    Selection selection1 = new Selection();
    selection1.setName("selection_1");
    selection1.setUuid("01");
    Selection selection2 = new Selection();
    selection2.setName("selection_2");
    selection2.setUuid("02");
    selectionRepository.save(selection);
    selectionRepository.save(selection1);
    selectionRepository.save(selection2);
    List<Selection> userSelection = Arrays.asList(selection,selection1);
    assertEquals(selectionRepository.findByUuid("01"),userSelection);
    assertFalse(selectionRepository.findByUuid("01").contains(selection2));
  }
  @Test
  void shouldFindByUUIDAndId(){
    Selection selection = new Selection();
    selection.setName("test_selection_id_uuid");
    selection.setUuid("01");
    selection = selectionRepository.save(selection);
    assertEquals(selectionRepository.findByIdAndUuid(selection.getId(),"01"),selection);
  }
  @Test
  void shouldDelete(){
    Selection selection = new Selection();
    selection.setName("test_selection_id_uuid");
    selection.setUuid("01");
    selection = selectionRepository.save(selection);
    selectionRepository.delete(selection);
    assertFalse(selectionRepository.findByUuid("01").contains(selection));
  }

}
