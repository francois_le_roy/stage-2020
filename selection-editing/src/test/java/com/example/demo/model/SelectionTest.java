package com.example.demo.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SelectionTest {
  private Selection selection;
  @BeforeEach
  void setup(){
    selection=new Selection();
  }
  @Test
  void shouldAddRecipeWhenRecipesIsEmpty(){
    Recipe recipe = new Recipe();
    selection.addRecipe(recipe);
    assertEquals(selection.getRecipes().get(0),recipe);
  }
  @Test
  void shouldAddRecipeWhenRecipesIsNotEmpty(){
    Recipe recipe = new Recipe();
    selection.addRecipe(recipe);
    Recipe recipe2 = new Recipe();
    selection.addRecipe(recipe2);
    assertEquals(selection.getRecipes().get(1),recipe2);
  }
  @Test
  void shouldRemoveRecipeIfRecipesNotEmpty(){
    Recipe recipe = new Recipe();
    recipe.setId("01");
    selection.addRecipe(recipe);
    selection.removeRecipe("01");
    assertEquals(Arrays.asList(),selection.getRecipes());
  }

}
