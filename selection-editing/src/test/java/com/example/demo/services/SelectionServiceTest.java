package com.example.demo.services;

import com.example.demo.model.Recipe;
import com.example.demo.model.Selection;
import com.example.demo.repository.RecipeRepository;
import com.example.demo.repository.SelectionRepository;
import com.example.demo.web.SelectionController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
@ActiveProfiles("test")
class SelectionServiceTest {
  @MockBean
  private SelectionRepository selectionRepository;
  @MockBean
  private RecipeRepository recipeRepository;
  private SelectionService selectionService;
  private String uuid;
  @BeforeEach
  void setup(){
    selectionRepository = mock(SelectionRepository.class);
    recipeRepository = mock(RecipeRepository.class);
    selectionService = new SelectionService(selectionRepository,recipeRepository);
    uuid = "01";
  }

  @Test
  void shouldReturnAllSelectionFromAnUser(){
    Selection selection = new Selection();
    selection.setName("test");
    selection.setUuid(uuid);
    Selection selection2 = new Selection();
    selection2.setName("test2");
    selection2.setUuid(uuid);
    List<Selection> selections = Arrays.asList(selection,selection2);
    when(selectionRepository.findByUuid(uuid)).thenReturn(selections);
    assertEquals(selections,selectionService.selections(uuid));
  }
  @Test
  void shouldReturnASelectionWithGivenId(){
    Selection selection = new Selection();
    selection.setId("01");
    selection.setName("test");
    selection.setUuid(uuid);
    List<Selection> selections = Arrays.asList(selection);
    when(selectionRepository.findByIdAndUuid("01",uuid)).thenReturn(selection);
    assertEquals(selection,selectionService.selection("01",uuid));
  }
  @Test
  void shouldReturnSelectionWhenSavingNewSelection(){
    Selection toSave = new Selection();
    toSave.setName("test");
    toSave.setUuid(uuid);
    when(selectionRepository.insert(toSave)).thenReturn(toSave);
    Selection saved = selectionService.storeOne(toSave);
    assertEquals(saved,toSave);
  }
  @Test
  void shouldReturnSelectionWhenSavingNewRecipeInSelection(){
    Recipe toSave = new Recipe();
    toSave.setName("test");
    toSave.setDescription("test");
    Selection target = new Selection();
    target.setName("target_selection");
    target.setUuid(uuid);
    target.setId("01");
    when(selectionRepository.findById("01")).thenReturn(java.util.Optional.of(target));
    when(selectionRepository.save(target)).thenReturn(target);
    when(recipeRepository.save(toSave)).thenReturn(toSave);
    Selection saved = selectionService.storeOneRecipe(toSave,"01");
    assertEquals(target,saved);
  }
  @Test
  void shouldReturnTrueIfDeleteSelectionWithSuccess(){
    Selection toDelete = new Selection();
    toDelete.setName("selection_to_delete");
    toDelete.setId("01");
    when(selectionRepository.findByIdAndUuid("01",uuid)).thenReturn(toDelete);
    assertTrue(selectionService.delete("01",uuid));
  }
  @Test
  void shouldReturnFalseIfDeleteSelectionWithFailure(){
    Selection toDelete = new Selection();
    toDelete.setName("selection_to_delete");
    toDelete.setId("01");
    when(selectionRepository.findByIdAndUuid("01",uuid)).thenReturn(toDelete);
    Mockito.doThrow(new RuntimeException()).when(selectionRepository).delete(toDelete);
    assertFalse(selectionService.delete("01",uuid));
  }
  @Test
  void shouldReturnTrueIfDeleteRecipeWithSuccess(){
    Selection toDelete = new Selection();
    toDelete.setName("selection_to_delete");
    toDelete.setId("01");
    when(selectionRepository.findByIdAndUuid("01",uuid)).thenReturn(toDelete);
    assertTrue(selectionService.delete("01",uuid));
  }
  @Test
  void shouldReturnFalseIfDeleteRecipeWithFailure(){
    Selection toDelete = new Selection();
    toDelete.setName("selection_to_delete");
    toDelete.setId("01");
    when(selectionRepository.findByIdAndUuid("01",uuid)).thenReturn(toDelete);
    Mockito.doThrow(new RuntimeException()).when(selectionRepository).delete(toDelete);
    assertFalse(selectionService.delete("01",uuid));
  }
  @Test
  void shouldDeleteManyRecipes(){

    Selection selection = new Selection();
    selection.setUuid("01");
    selection.setName("selection_test");
    Recipe recipe1 = new Recipe();
    Recipe recipe2 = new Recipe();
    Recipe recipe3 = new Recipe();
    Recipe recipe4 = new Recipe();
    recipe1.setName("recipe1");
    recipe2.setName("recipe2");
    recipe3.setName("recipe3");
    recipe4.setName("recipe4");
    recipe1.setId("01");
    recipe2.setId("02");
    recipe3.setId("03");
    recipe4.setId("04");
    selection.addRecipe(recipe1);
    selection.addRecipe(recipe2);
    selection.addRecipe(recipe3);
    selection.addRecipe(recipe4);
    selection.setName("selection_to_delete");
    selection.setId("01");
    selection.setUuid("00");
    when(selectionRepository.findByIdAndUuid("01","00")).thenReturn(selection);


    for (int i =1; i<5;i++){
      selectionService.deleteRecipe("01","0"+i,"00");
    }
    assertTrue(selection.getRecipes().isEmpty());
  }

}
