package com.example.demo.web;

import com.example.demo.model.Recipe;
import com.example.demo.model.Selection;
import com.example.demo.services.SelectionService;
import com.example.demo.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.bytebuddy.asm.Advice;
import org.apache.coyote.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith({MockitoExtension.class})
@WebMvcTest(SelectionController.class)
@ActiveProfiles("test")
@EnableWebMvc
@AutoConfigureMockMvc
class SelectionControllerTest {
  @Autowired
  private WebApplicationContext webApplicationContext;
  @MockBean
  private SelectionService selectionService;
  @MockBean
  private UserService userService;
  @Autowired
  private MockMvc mockMvc;

  private Selection selection;

  private Selection selection2;

  private  String uuid = "01";

  @BeforeEach
  void setup() {
    mockMvc= MockMvcBuilders.webAppContextSetup(webApplicationContext)
                             .apply(springSecurity()).build();


    selection = new Selection();
    selection.setName("nouvelle_selection");
    selection.setId("01");
    selection.setUuid(uuid);

    selection2 = new Selection();
    selection2.setName("nouvelle_selection_2");
    selection2.setId("02");
    selection2.setUuid(uuid);
    List<Selection> selections = Arrays.asList(selection,selection2);

    given(selectionService.selections(uuid)).willReturn(selections);
    given(selectionService.selection("01",uuid)).willReturn(selection);
    given(selectionService.selection("02",uuid)).willReturn(selection2);
    given(userService.getUserId()).willReturn(uuid);
  }
  @Test
  @WithMockUser(roles = "USER")
  void shouldListAllSelectionsForAnUser() throws Exception {
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/selection");

    mockMvc.perform(requestBuilder)
      .andExpect(MockMvcResultMatchers.status().isOk())
      .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value("01"))
      .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("nouvelle_selection"))
      .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value("02"))
      .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("nouvelle_selection_2"));
  }

  @Test
  @WithMockUser(roles = "USER")
  void shouldReturnOneSelectionWithId() throws Exception {
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/selection/01");
    mockMvc.perform(requestBuilder)
      .andExpect(MockMvcResultMatchers.status().isOk())
      .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(MockMvcResultMatchers.jsonPath("id").value("01"))
      .andExpect(MockMvcResultMatchers.jsonPath("name").value("nouvelle_selection"));

  }
  @Test
  @WithMockUser(roles = "USER")
  void shouldAddNewSelection() throws Exception {
    Selection toPost = new Selection();
    toPost.setId("03");
    toPost.setName("ma_selection");

    given(selectionService.storeOne(any())).willReturn(toPost);

    ObjectMapper mapper = new ObjectMapper();
    String requestBody = mapper.writeValueAsString(toPost);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/selection")
      .contentType(MediaType.APPLICATION_JSON)
      .content(requestBody);
    mockMvc.perform(requestBuilder)
      .andExpect(MockMvcResultMatchers.status().isCreated());
  }
  @Test
  @WithMockUser(roles = "USER")
  void shouldAddNewRecipe() throws Exception {
    Recipe toPost = new Recipe();
    toPost.setName("test_recette");
    toPost.setDescription("test_recette");
    List<Recipe> recipes = Arrays.asList(toPost);
    given(selectionService.storeOneRecipe(any(),eq("01"))).willReturn(selection);
    ObjectMapper mapper = new ObjectMapper();
    String requestBody = mapper.writeValueAsString(recipes);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/selection/01/recipe")
      .contentType(MediaType.APPLICATION_JSON)
      .content(requestBody);
    System.out.println("here "+mockMvc.perform(requestBuilder).andReturn().getResponse().getHeader("Location"));
    mockMvc.perform(requestBuilder)
      .andExpect(MockMvcResultMatchers.status().isCreated());
  }
  @Test
  @WithMockUser(roles = "USER")
  void shouldReturnSuccessOnDeleteSelectionSuccess() throws Exception {
    given(selectionService.delete(eq("01"),eq(uuid))).willReturn(true);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/selection/01");
    mockMvc.perform(requestBuilder)
      .andExpect(MockMvcResultMatchers.status().isOk());
  }
  @Test
  @WithMockUser(roles = "USER")
  void shouldReturnNotFoundOnDeleteSelectionFailure() throws Exception {
    given(selectionService.delete(eq("01"),eq(uuid))).willReturn(false);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/selection/01");
    mockMvc.perform(requestBuilder)
      .andExpect(MockMvcResultMatchers.status().isNotFound());
  }

  @Test
  @WithMockUser(roles = "USER")
  void shouldReturnSuccessOnDeleteRecipeSuccess() throws Exception {
    given(selectionService.deleteRecipe(eq("01"),eq("01"),eq(uuid))).willReturn(true);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/selection/01/recipe/01");
    mockMvc.perform(requestBuilder)
      .andExpect(MockMvcResultMatchers.status().isOk());
  }
  @Test
  @WithMockUser(roles = "USER")
  void shouldReturnNotFoundOnDeleteRecipeFailure() throws Exception {
    given(selectionService.deleteRecipe(eq("01"),eq("01"),eq(uuid))).willReturn(false);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/selection/01/recipe/01");
    mockMvc.perform(requestBuilder)
      .andExpect(MockMvcResultMatchers.status().isNotFound());
  }
}
