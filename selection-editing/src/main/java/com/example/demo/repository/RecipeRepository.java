package com.example.demo.repository;

import com.example.demo.model.Recipe;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RecipeRepository extends MongoRepository<Recipe,String> {
  void deleteById(String id);
}
