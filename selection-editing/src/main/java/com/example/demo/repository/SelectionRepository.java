package com.example.demo.repository;
import com.example.demo.model.Selection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SelectionRepository extends MongoRepository<Selection,String>{
    List<Selection> findByUuid(String uuid);
    Selection findByIdAndUuid(String id,String uuid);
}
