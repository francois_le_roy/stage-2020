package com.example.demo.services;

import com.example.demo.model.Recipe;
import com.example.demo.model.Selection;
import com.example.demo.repository.RecipeRepository;
import com.example.demo.repository.SelectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class SelectionService {

    private SelectionRepository selectionRepository;
    private RecipeRepository recipeRepository;

    public SelectionService(SelectionRepository selectionRepository,RecipeRepository recipeRepository){
      this.selectionRepository = selectionRepository;
      this.recipeRepository=recipeRepository;
    }
    public List<Selection> selections(String uuid) {
        return selectionRepository.findByUuid(uuid);
    }

    public Selection selection(String selectionId,String uuid) {
      return selectionRepository.findByIdAndUuid(selectionId,uuid);
    }
    public Selection storeOne(Selection selection){
        return selectionRepository.insert(selection);
    }

    public Selection storeOneRecipe(Recipe recipe, String selectionId) {
        Optional<Selection> chosenSelection = selectionRepository.findById(selectionId);
        Recipe savedRecipe = recipeRepository.save(recipe);
        chosenSelection.get().addRecipe(savedRecipe);
        return selectionRepository.save(chosenSelection.get());
    }
    public boolean delete(String selectionId,String uuid){
      Selection chosenSelection = selectionRepository.findByIdAndUuid(selectionId,uuid);
      try {
        selectionRepository.delete(chosenSelection);
        return true;
      }
      catch (Exception e){
        e.printStackTrace();
        return false;
      }
    }
    public boolean deleteRecipe(String selectionId, String recipeId,String uuid){
      Selection chosenSelection = selectionRepository.findByIdAndUuid(selectionId,uuid);
      try {
        recipeRepository.deleteById(recipeId);
        chosenSelection.removeRecipe(recipeId);
        selectionRepository.save(chosenSelection);
        return true;
      }
      catch (Exception e){
        e.printStackTrace();
        return false;
      }
    }
}
