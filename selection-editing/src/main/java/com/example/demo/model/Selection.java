package com.example.demo.model;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Data
@Document
@CompoundIndexes({
  @CompoundIndex(name = "userId_name", def = "{'uuid' : 1, 'name': 1}",unique = true)
})
public class Selection {

    @Id
    private String id;
    @Field
    private String name;
    @DBRef
    private List<Recipe> recipes;
    @Field
    private String uuid;
    public void addRecipe(Recipe recipe){
        if (this.recipes==null){
            this.recipes= new ArrayList<>();
            this.recipes.add(recipe);
        }
        else {
            this.recipes.add(recipe);
        }
    }
  public void removeRecipe(String recipeId) {
    if (this.recipes!=null){
      this.recipes.removeIf(recipeToDelete -> recipeToDelete.getId().equals(recipeId));
    }
  }
}

