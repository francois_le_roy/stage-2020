package com.example.demo.model;

import lombok.Data;

@Data
public class Ingredient {
  private String key;
  private String name;
}
