package com.example.demo.model;

import lombok.Data;

@Data
public class Unit {
  private String key;
  private String name;
}
