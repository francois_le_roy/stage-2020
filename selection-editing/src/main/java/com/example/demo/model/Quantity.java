package com.example.demo.model;

import lombok.Data;

@Data
public class Quantity {
  private Ingredient ingredient;
  private Unit unit;
  private int quantity;
}
