package com.example.demo.web;

import com.example.demo.model.Recipe;
import com.example.demo.model.Selection;
import com.example.demo.services.SelectionService;
import com.example.demo.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class SelectionController {
    private static final Logger logger = LoggerFactory.getLogger(SelectionController.class);
    private final SelectionService selectionService;
    private final UserService userService;


    public SelectionController(SelectionService selectionService, UserService userService){
        this.selectionService = selectionService;
        this.userService = userService;
    }

    @GetMapping("/selection")
    public ResponseEntity<List<Selection>> listAllSelection(){
        String uuid = userService.getUserId();
        return ResponseEntity.ok(selectionService.selections(uuid));

    }
    @GetMapping("/selection/{selectionId}" )
    public ResponseEntity<Selection> getSelection(@PathVariable final String selectionId) {
        logger.info("get by name");
      String uuid = userService.getUserId();
      return ResponseEntity.ok(selectionService.selection(selectionId,uuid));
    }
    @PostMapping("/selection")
    public ResponseEntity<Selection> addSelection(@RequestBody Selection selection)  {
        logger.info("post one ");
        String uuid = userService.getUserId();
        selection.setUuid(uuid);
        Selection postedSelection = selectionService.storeOne(selection);
        URI location = URI.create(String.format("/selection/%s", postedSelection.getId()));
        return ResponseEntity.created(location).build();

    }
    @PostMapping(value = "/selection/{selectionId}/recipe")
    public ResponseEntity addRecipe(@PathVariable final String selectionId,@RequestBody Recipe[] recipes) {
      logger.info("post recipes in selection id");
      Selection chosenSelection = new Selection();
      for (Recipe recipeToAdd:recipes) {
        chosenSelection = selectionService.storeOneRecipe(recipeToAdd,selectionId);
      }
      URI location = URI.create(String.format("/selection/%s", chosenSelection.getId()));
      System.out.println("location "+ ResponseEntity.created(location).build());
      return ResponseEntity.created(location).build();
    }

    @DeleteMapping(value = "/selection/{selectionId}")
    public ResponseEntity<String> removeSelection(@PathVariable final String selectionId){
        logger.info("delete selection");
        String uuid = userService.getUserId();
        boolean isDeleted = selectionService.delete(selectionId,uuid);
        if (isDeleted){
          return ResponseEntity.ok(selectionId);
        }
        else {
          return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    @DeleteMapping(value = "/selection/{selectionId}/recipe/{recipeId}")
    public ResponseEntity<String> removeRecipe(@PathVariable final String selectionId,@PathVariable final String recipeId){
      logger.info("delete recipe in selection");

      String uuid = userService.getUserId();
      boolean isDeleted = selectionService.deleteRecipe(selectionId,recipeId,uuid);
      if (isDeleted){
        return ResponseEntity.ok(selectionId);
      }
      else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    }

}
