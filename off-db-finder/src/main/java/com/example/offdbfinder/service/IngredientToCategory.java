package com.example.offdbfinder.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Map;
@Component
public class IngredientToCategory {
  private Map<String,String[]> ingredientToCategoryMap;
  public IngredientToCategory(){
    ObjectMapper mapper = new ObjectMapper();
    try {

      ingredientToCategoryMap = mapper.readValue(new File(
        "src/main/resources/ingredientToCategory.json"), new TypeReference<Map<String, String[]>>() {
      });
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  public String[] mapToCategory(String ingredient_name) {
    return ingredientToCategoryMap.get(ingredient_name);
  }
}
