package com.example.offdbfinder.service;

import com.example.offdbfinder.model.Product;
import com.example.offdbfinder.model.ProductList;
import com.example.offdbfinder.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ProductService {
  @Autowired
  private ProductRepository productRepository;
  @Autowired
  private IngredientToCategory ingredientToCategory;
  @Autowired
  private FilterOnSearch filterOnSearch;

  public ProductList findByCategory(String ingredient_key, String ingredient_name,int quantity, String unit, String store, Pageable p){
    String[] categories  = this.ingredientToCategory.mapToCategory(ingredient_key);
    if (categories!=null){
      List<Product> candidates = filterOnSearch.filterOnSearch(ingredient_name,quantity,unit,store,categories,p) ;
      checkCandidateIsValid(candidates);
      ProductList candidateList = new ProductList();
      candidateList.setProducts(candidates);
      return  candidateList;
    }
    return new ProductList();
  }

  private void checkCandidateIsValid(List<Product> candidates) {
    List<Product> invalidProducts = new ArrayList<>();
    for (Product product: candidates){
      if (product.getProduct_name()==null){
        invalidProducts.add(product);
      }
      else {
        if (product.getProduct_name().equals("")){
          invalidProducts.add(product);
        }
      }
    }
    candidates.removeAll(invalidProducts);
  }
}
