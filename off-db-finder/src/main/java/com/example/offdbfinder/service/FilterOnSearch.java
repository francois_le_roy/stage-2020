package com.example.offdbfinder.service;

import com.example.offdbfinder.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FilterOnSearch {
  @Autowired
  private MongoTemplate mongoTemplate;

  List<Product> filterOnSearch( String ingredient_name, int quantity,String unit,String store,String[] categories, Pageable p){
    TextCriteria criteria = TextCriteria.forLanguage("fr")
      .matchingAny(ingredient_name);
    Criteria storeCriteria = new Criteria();
    Query query = TextQuery.queryText(criteria).sortByScore()
      .addCriteria(Criteria.where("categories_tags").in(categories))
      //.addCriteria(Criteria.where("quantity").gte(quantity))
      /*.addCriteria(storeCriteria.orOperator(
        Criteria.where("storesTags").in(List.of(store,store.toLowerCase(),"")),
        Criteria.where("storeTags").exists(false)
      ))*/
      .addCriteria(Criteria.where("storesTags").in(List.of(store,store.toLowerCase(),"")))
      .with(p);
    log.info(ingredient_name);
    log.info("before");
    List<Product> candidateFiltered = mongoTemplate.find(query, Product.class);
    log.info("after");
    return candidateFiltered;
  }
}
