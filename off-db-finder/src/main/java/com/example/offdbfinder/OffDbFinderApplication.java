package com.example.offdbfinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OffDbFinderApplication {

  public static void main(String[] args) {
    SpringApplication.run(OffDbFinderApplication.class, args);
  }

}
