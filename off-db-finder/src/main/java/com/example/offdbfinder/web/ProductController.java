package com.example.offdbfinder.web;

import com.example.offdbfinder.model.Product;
import com.example.offdbfinder.model.ProductList;
import com.example.offdbfinder.repository.ProductRepository;
import com.example.offdbfinder.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {
  private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
  @Autowired
  private ProductService productService;
  @GetMapping(path = "/products/{ingredient_key}/{ingredient_name}/{quantity}/{unit}/{store}")
  public ResponseEntity<ProductList> GetProductByCategory(@PathVariable String ingredient_key, Pageable p, @PathVariable String ingredient_name, @PathVariable int quantity, @PathVariable String unit, @PathVariable String store){
    logger.info("get product by category");
    return ResponseEntity.ok(this.productService.findByCategory(ingredient_key,ingredient_name,quantity,unit,store,p));
  }
}
