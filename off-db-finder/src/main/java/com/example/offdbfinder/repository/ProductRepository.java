package com.example.offdbfinder.repository;


import com.example.offdbfinder.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

@RepositoryRestResource(collectionResourceRel="products",path="products")
public interface ProductRepository extends PagingAndSortingRepository<Product, String> {
  List<Product> findByCategoriesTagsIn(@Param("categories") String[] categories);
}
