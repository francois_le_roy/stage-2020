package com.example.offdbfinder.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
public class ProductList {
  @Field
  public List<Product> products;
}
