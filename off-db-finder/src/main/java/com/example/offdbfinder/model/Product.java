package com.example.offdbfinder.model;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.math.BigInteger;

@Data
@Document(collection = "products")
public class Product {
  @Id
  private String  id;
  @Field
  @TextIndexed(weight = 1) private String ingredients_text_fr;
  @Field
  @TextIndexed(weight = 3) private String product_name;
  @Field
  private String generic_name;
  @Field
  private String code;
  @Field
  private String url;
  @Field
  @TextIndexed(weight = 1)
  private String[] categories;
  @Field("categories_tags")
  @TextIndexed(weight = 2)
  private String[] categoriesTags;
  @Field
  private String quantity;
  @Field("stores_tags")
  private String[] storesTags;
  @Override
  public boolean equals(Object o){
    if(o instanceof Product){
      return ((Product) o).getId().equals(this.id);
    }
    return false;
  }
}
