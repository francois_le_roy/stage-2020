import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ShoppingComponent} from "./shopping-details/shopping.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatButtonModule} from "@angular/material/button";
import {MatSelectModule} from "@angular/material/select";
import {DisplaySelectionComponent} from "../selection/display-selection/display-selection.component";
import {DisplayShoppingListComponent} from "./display-shopping-list/display-shopping-list.component";
import {MatSortModule} from "@angular/material/sort";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    ShoppingComponent,
    DisplayShoppingListComponent
  ],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonModule,
        MatSelectModule,
        MatSortModule,
        MatInputModule,
        MatIconModule,
        MatCheckboxModule,
        ReactiveFormsModule
    ],
  exports:[
    ShoppingComponent,
    DisplayShoppingListComponent
  ]
})
export class ShoppingModule { }
