import {Component, OnInit, ViewChild} from '@angular/core';
import {Product, Proposal, Quantity, Selection, ShoppingList} from "../../../model/selection";
import {ActivatedRoute, Router} from "@angular/router";
import {ShoppingService} from "../../core/services/shopping.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {DialogService} from "../../core/services/dialog.service";
import {FormControl, FormGroup} from "@angular/forms";
import {HttpResponse} from "@angular/common/http";

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.scss']
})
export class ShoppingComponent implements OnInit {
  shoppingList: ShoppingList;
  shoppingForm = new FormGroup({
    quantity: new FormControl(''),
  });
  displayedColumns: string[] = ['ingredient','unity','product','quantity','delete'];
  toDelete:Quantity[] = [];
  dataSource: MatTableDataSource<Quantity>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private shoppingService:ShoppingService,
    public dialogService:DialogService
  ) { }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');

    this.shoppingForm.setValue({quantity:1})
    this.shoppingService.getShoppingList(id).subscribe(
      (res: ShoppingList)=>
      {
        this.shoppingList= res
        this.countShoppingList()
        this.dataSource = new MatTableDataSource(this.shoppingList.baseList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
  }


  countShoppingList() {
    for (let proposal of this.shoppingList.proposals){
      if (proposal.product!=null){
        proposal.product.count = 1;
      }
    }
  }
  getProductForQuantity(element: Quantity) {

    for (let proposal of this.shoppingList.proposals){
      if (proposal.quantity.ingredient.key == element.ingredient.key){
        return proposal.product
      }
    }
  }

  addToRemoveList(row: any, checked: boolean) {
    if(checked){
      this.toDelete.push(row)
    }
    else{
      let index = this.toDelete.indexOf(row)
      this.toDelete.splice(index,1);
    }
  }

  deleteSelection() {
    let id = this.route.snapshot.paramMap.get('id');
    for(let quantity of this.toDelete){
      this.shoppingService.deleteCandidate(quantity,id).subscribe(
        ()=> this.ngOnInit()
      )
    }
  }

  requestCommand() {
    let products = []
    for (let proposal of this.shoppingList.proposals){
      if (proposal.product!=null){
        products.push(proposal.product)
      }
    }
    this.shoppingService.postListToShop(products).subscribe((res:HttpResponse<any>)=>
      {
        this.dialogService.openCommandDialog(res.body);
      }
    )
  }

  onSubmit(quantity: any) {
    let product = this.getProductForQuantity(quantity)
    product.count=this.shoppingForm.get("quantity").value
  }
}
