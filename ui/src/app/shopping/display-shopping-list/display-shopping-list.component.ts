import {Component, OnInit, ViewChild} from '@angular/core';
import {Proposal, Selection, ShoppingList} from "../../../model/selection";
import {ShoppingService} from "../../core/services/shopping.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Router} from "@angular/router";

@Component({
  selector: 'app-display-shopping-list',
  templateUrl: './display-shopping-list.component.html',
  styleUrls: ['./display-shopping-list.component.scss']
})
export class DisplayShoppingListComponent implements OnInit {
  shoppingLists:ShoppingList[]
  dataSource: MatTableDataSource<ShoppingList>;
  displayedColumns: string[] = ['name','store','nbProduct'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(
    private shoppingService:ShoppingService,
    private router:Router,
  ) {
    this.initShoppingListComponent()
  }

  ngOnInit(): void {

  }

  navigateTo(row: any) {
    this.router.navigateByUrl("/shopping/"+row.id)
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private initShoppingListComponent() {
    this.shoppingService.getShoppingLists().subscribe(
      (res:ShoppingList[])=>{
        this.shoppingLists = res;
        this.dataSource = new MatTableDataSource(this.shoppingLists);
        console.log(this.dataSource);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    )
  }
  countProductInProposalsMap(shoppingList){
    let index =this.shoppingLists.indexOf(shoppingList)
    return this.shoppingLists[index].proposals.length
  }

}
