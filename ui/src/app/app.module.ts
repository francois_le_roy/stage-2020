import {NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from "@angular/common/http";
import { OAuthModule } from 'angular-oauth2-oidc';

import { AppRoutingModule } from './app-routing.module';
import {HomepageModule} from "./homepage/connexion-home/homepage.module";
import {SharedModule} from "./shared/shared.module";
import {AppComponent} from "./app.component";
import {CoreModule} from "./core/core.module";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatDividerModule} from "@angular/material/divider";
import {MatListModule} from "@angular/material/list";
import { SelectionModule } from './selection/selection.module';
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {environment} from "../environments/environment";
import {AuthGuardService} from "./auth-guard.service";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {ShoppingModule} from "./shopping/shopping.module";
import {MatIconModule} from "@angular/material/icon";
import {ChooseProductModule} from "./choose-product/choose-product.module";


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    HomepageModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['http://localhost:8081/', 'http://localhost:8082', 'http://localhost:8083'],//, 'http://localhost:8001/lists'],
        sendAccessToken: true
      },
    }),
    SelectionModule,
    MatToolbarModule,
    MatDividerModule,
    MatListModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    ShoppingModule,
    MatIconModule,
    ChooseProductModule
  ],
  bootstrap: [AppComponent],
  providers: [AuthGuardService]
})
export class AppModule { }
