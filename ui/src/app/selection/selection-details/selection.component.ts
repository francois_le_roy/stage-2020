import {Component, Inject, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {Recipe, Selection, SelectionAndStore} from "../../../model/selection";
import {SelectionService} from "../../core/services/selection.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {RecipeSearchComponent} from "../../shared/recipe-search/recipe-search.component";
import {SearchRecipeService} from "../../core/services/search-recipe.service";
import {ShoppingService} from "../../core/services/shopping.service";
import {ChooseStoreComponent} from "../../shared/choose-store/choose-store.component";
import {DialogService} from "../../core/services/dialog.service";

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss']
})
export class SelectionComponent implements OnInit {
  selection: Selection;
  recipesToRemove: Recipe[] = [];

  constructor(
    private route:ActivatedRoute,
    private selectionService: SelectionService,
    private recipeService:SearchRecipeService,
    private shoppingService:ShoppingService,
    public dialogService:DialogService
  ) {}

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.selectionService.getSelection(id).subscribe((res: Selection)=>this.selection= res);
  }
  ngOnChange(changes: SimpleChanges){
  }
  ngAfterViewInit(){
  }

  addToRemoveList(recipe: any, checked: boolean) {
    if (checked){this.recipesToRemove.push(recipe)}
    else {
      let index = this.recipesToRemove.indexOf(recipe)
      this.recipesToRemove.splice(index,1)
    }
  }

  deleteRecipes() {
    let id = this.route.snapshot.paramMap.get('id');
    for (let recipe of this.recipesToRemove){
      let index =this.selection.recipes.indexOf(recipe);
      console.log(index)
      this.selection.recipes.splice(index,1);
      this.recipeService.deleteRecipe(recipe,id,recipe.id).subscribe()
    }
    this.recipesToRemove=[]
  }

  callShoppingRequest() {
      let selectionToConvert = new SelectionAndStore();
      selectionToConvert.selection = this.selection;
      selectionToConvert.store = "";
      this.shoppingService.postSelectionToShop(selectionToConvert);

  }
}
