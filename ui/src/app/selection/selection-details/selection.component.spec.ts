import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionComponent } from './selection.component';
import {SelectionService} from "../../core/services/selection.service";
import {Observable, of} from "rxjs";
import {ActivatedRoute, convertToParamMap, Params} from "@angular/router";

describe('SelectionComponent', () => {
  let component: SelectionComponent;
  let fixture: ComponentFixture<SelectionComponent>;
  let mockSelectionService;

  class MockSelectionService{
    getSelection(){
      return of('{name:maselection}')
    }
  }

  beforeEach(async(() => {
    mockSelectionService = jasmine.createSpyObj(['getSelection']);
    TestBed.configureTestingModule({
      declarations: [SelectionComponent],
      providers:[
        SelectionComponent,
        {provide:SelectionService,useClass:MockSelectionService},
        SelectionComponent,
        {provide: ActivatedRoute, useValue: {snapshot: {paramMap:{get:()=>1}}}}
    ]
    });
    TestBed.compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionComponent);
    component = fixture.componentInstance;
    console.log(mockSelectionService.getSelection())
    fixture.detectChanges();
  });

  it('should create"', () => {
    expect(component).toBeTruthy();
  });
});
