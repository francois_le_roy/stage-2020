import {Component, OnInit, ViewChild} from '@angular/core';
import {SelectionService} from "../../core/services/selection.service";
import {Recipe, Selection} from "../../../model/selection";
import {Router} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {CreateSelectionComponent} from "../../shared/create-selection/create-selection.component";
import {MatDialog} from "@angular/material/dialog";
import {DialogService} from "../../core/services/dialog.service";

@Component({
  selector: 'app-display-selection',
  templateUrl: './display-selection.component.html',
  styleUrls: ['./display-selection.component.scss']
})
export class DisplaySelectionComponent implements OnInit {
  selections: Selection[]
  selectionsToRemove:Selection[] = []
  displayedColumns: string[] = ['name','nbRecipe','delete'];
  dataSource: MatTableDataSource<Selection>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public dialog:MatDialog,
    private selectionService: SelectionService,
    private router:Router,
    public dialogService:DialogService
  ) {
    this.initSelectionTable()

  }

  ngOnInit(): void {

  }

  navigateTo(row: any) {
    this.router.navigateByUrl("/selection/"+row.id)
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addToRemoveList(row: Selection,e) {
    if(e){
      this.selectionsToRemove.push(row)
    }
    else{
      let index = this.selectionsToRemove.indexOf(row)
      this.selectionsToRemove.splice(index,1);
    }
  }

  removeSelection() {
    this.selectionService.removeMany(this.selectionsToRemove).subscribe(
      ()=>{
        this.initSelectionTable()
        this.selectionsToRemove=[]
      }
    )

  }

  initSelectionTable(){
    this.selectionService.getSelections().subscribe((res: Selection[])=>
      {
        this.selections= res
        this.dataSource = new MatTableDataSource(this.selections);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    )
  }
}
