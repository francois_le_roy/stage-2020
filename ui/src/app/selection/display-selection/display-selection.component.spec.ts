import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplaySelectionComponent } from './display-selection.component';
import {SelectionService} from "../../core/services/selection.service";
import {Router} from "@angular/router";
import {AppTopBarComponent} from "../../core/components/app-top-bar/app-top-bar.component";
import {Observable} from "rxjs";

describe('DisplaySelectionComponent', () => {
  let component: DisplaySelectionComponent;
  let fixture: ComponentFixture<DisplaySelectionComponent>;
  let selectionService;
  let router;
  class MockSelectionService{
    getSelections(){
      return new Observable()
    }
  }
  class MockRouter{
    navigateByUrl(){}
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplaySelectionComponent ],
      providers:[
      DisplaySelectionComponent,{
        provide:SelectionService,useClass:MockSelectionService
      },
      DisplaySelectionComponent,{
        provide:Router,useClass:MockRouter
      }
      ]
    })
    component = TestBed.inject(DisplaySelectionComponent);
    selectionService = TestBed.inject(SelectionService);
    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(DisplaySelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
