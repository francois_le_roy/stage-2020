import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CreateSelectionComponent} from "../shared/create-selection/create-selection.component";
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {DisplaySelectionComponent} from "./display-selection/display-selection.component";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {RouterModule} from "@angular/router";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatListModule} from "@angular/material/list";
import {SelectionComponent} from "./selection-details/selection.component";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatCardModule} from "@angular/material/card";



@NgModule({
  declarations: [
    DisplaySelectionComponent,
    SelectionComponent
  ],
    imports: [
        CommonModule,
        MatInputModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonToggleModule,
        RouterModule,
        MatExpansionModule,
        MatListModule,
        MatCheckboxModule,
        MatCardModule

    ],
  exports: [
    DisplaySelectionComponent,
    SelectionComponent
  ]
})
export class SelectionModule { }
