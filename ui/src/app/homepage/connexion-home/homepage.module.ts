import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomepageComponent} from "./homepage.component";
import {UserHomeModule} from "../user-home/user-home.module";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    HomepageComponent,
  ],
  imports: [
    CommonModule,
    UserHomeModule,
    MatGridListModule,
    MatCardModule,
    MatIconModule,
    RouterModule
  ],
  exports:[
    HomepageComponent,
  ]
})
export class HomepageModule { }
