import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageComponent } from './homepage.component';
import {AuthService} from "../../core/services/auth.service";

describe('HomepageComponent', () => {
  let component: HomepageComponent;
  let authService: AuthService;
  let fixture: ComponentFixture<HomepageComponent>;
  class MockAuthService{
    claims = null;
    get isLoggedIn(){return !!this.claims}
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations:[HomepageComponent],
      providers:[HomepageComponent,
          {
            provide: AuthService,
            useClass: MockAuthService
          }
        ]
    })
    .compileComponents();
    component = TestBed.inject(HomepageComponent);
    authService = TestBed.inject(AuthService);
    fixture = TestBed.createComponent(HomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
