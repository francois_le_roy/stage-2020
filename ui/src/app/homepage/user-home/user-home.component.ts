import { Component, OnInit } from '@angular/core';
import {SelectionService} from "../../core/services/selection.service";
import {MatDialog} from "@angular/material/dialog";
import {RecipeSearchComponent} from "../../shared/recipe-search/recipe-search.component";
import {CreateSelectionComponent} from "../../shared/create-selection/create-selection.component";
import {DialogService} from "../../core/services/dialog.service";

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss']
})
export class UserHomeComponent implements OnInit {

  constructor(
    public dialogService:DialogService
  ) { }

  ngOnInit(): void {
  }

}
