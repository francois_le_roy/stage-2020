import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserHomeComponent} from "./user-home.component";
import {MatListModule} from "@angular/material/list";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {AppRoutingModule} from "../../app-routing.module";



@NgModule({
  declarations: [
    UserHomeComponent
  ],
  imports: [
    CommonModule,
    MatListModule,
    MatExpansionModule,
    MatButtonToggleModule,
    AppRoutingModule
  ],
  exports:[
    UserHomeComponent
  ]
})
export class UserHomeModule { }
