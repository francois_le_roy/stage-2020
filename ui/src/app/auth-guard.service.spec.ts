import { TestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import {AuthService} from "./core/services/auth.service";
import {Router} from "@angular/router";

describe('AuthGuardService', () => {
  let service: AuthGuardService;
  let router;
  let authService;


  class MockAuthService{
    claims = null;
    get isLoggedIn(){return !!this.claims}
  }
  class MockRouter{
    navigateByUrl(){}
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        AuthGuardService,  {
          provide: AuthService, useClass: MockAuthService
        },
        AuthGuardService, {
          provide: Router,useClass: MockRouter
        }
      ]
    });
    service = TestBed.inject(AuthGuardService);
    router = TestBed.inject(Router);
    authService = TestBed.inject(AuthService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
