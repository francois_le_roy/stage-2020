import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {map, startWith} from "rxjs/operators";
import {Selection, SelectionAndStore} from "../../../model/selection";
import {ShoppingService} from "../../core/services/shopping.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CreateSelectionComponent} from "../create-selection/create-selection.component";
import {HttpResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-choose-store',
  templateUrl: './choose-store.component.html',
  styleUrls: ['./choose-store.component.css']
})
export class ChooseStoreComponent implements OnInit {
  chooseStoreForm= new FormGroup({
  store: new FormControl(''),
  });
  options: string[] = ['Leclerc', 'Auchan', 'Carrefour'];
  filteredOptions: Observable<string[]>;
  private selection:Selection;
  isLoading:boolean
  constructor(
    private router:Router,
    private shoppingService: ShoppingService,
    public dialogRef: MatDialogRef<ChooseStoreComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.isLoading=false
    this.selection = data.selection;
    this.filteredOptions = this.chooseStoreForm.controls["store"].valueChanges.pipe(
    startWith(''),
    map(value => this._filter(value))
  );}

  ngOnInit(): void {
  }

  onSubmit() {
    let selectionAndStore = new SelectionAndStore();
    selectionAndStore.selection = this.selection;
    selectionAndStore.store = this.chooseStoreForm.get("store").value;
    this.isLoading=true
    this.shoppingService.postSelectionToShop(selectionAndStore).subscribe(
      (res)=>{
        this.dialogRef.close()
        this.isLoading=false
        let location = res.headers.get("location");
        this.router.navigateByUrl(location)
      }
    )
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
}
