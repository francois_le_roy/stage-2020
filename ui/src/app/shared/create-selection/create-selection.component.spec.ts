import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSelectionComponent } from './create-selection.component';
import {of} from "rxjs";
import {SelectionService} from "../../core/services/selection.service";
import {SelectionComponent} from "../../selection/selection-details/selection.component";

describe('CreateSelectionComponent', () => {
  let component: CreateSelectionComponent;
  let fixture: ComponentFixture<CreateSelectionComponent>;
  let mockSelectionService;

  class MockSelectionService{
    getSelections(){
      return of('{name:maselection}')
    }
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSelectionComponent ],
      providers:[
        SelectionComponent,
        {provide:SelectionService,useClass:MockSelectionService}
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
