import { Component, OnInit } from '@angular/core';
import {SelectionService} from "../../core/services/selection.service";
import {FormControl, FormGroup} from '@angular/forms';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-create-selection',
  templateUrl: './create-selection.component.html',
  styleUrls: ['./create-selection.component.scss']
})
export class CreateSelectionComponent implements OnInit {
  selectionForm = new FormGroup({
    name: new FormControl(''),
  });
  constructor(
    public dialogRef: MatDialogRef<CreateSelectionComponent>,
    readonly selectionService:SelectionService
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    let selectionToPost = JSON.stringify(this.selectionForm.value);
    this.selectionService.postSelection(selectionToPost);
    this.dialogRef.close();
  }
}
