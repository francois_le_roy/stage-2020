import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {RecipeResultsComponent} from "./recipe-search/recipe-results/recipe-results.component";
import {RecipeSearchComponent} from "./recipe-search/recipe-search.component";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatListModule} from "@angular/material/list";
import {MatTableModule} from "@angular/material/table";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatTabsModule} from "@angular/material/tabs";
import {CreateSelectionComponent} from "./create-selection/create-selection.component";
import {ChooseStoreComponent} from "./choose-store/choose-store.component";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import { CommandComponent } from './command/command.component';
import {MatButtonToggleModule} from "@angular/material/button-toggle";


@NgModule({
  declarations: [
    RecipeResultsComponent,
    RecipeSearchComponent,
    CreateSelectionComponent,
    ChooseStoreComponent,
    CommandComponent
  ],
  imports: [
    CommonModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatListModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatProgressBarModule,
    MatButtonToggleModule
  ],
})
export class SharedModule {
  constructor() {
  }
}
