import {Component, Inject, OnInit} from '@angular/core';
import {ShoppingService} from "../../core/services/shopping.service";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {CommandedProduct, CommandedShoppingList, Product} from "../../../model/selection";

@Component({
  selector: 'app-command',
  templateUrl: './command.component.html',
  styleUrls: ['./command.component.scss']
})
export class CommandComponent implements OnInit {
  id : string
  total:number
  products:CommandedProduct[]
  constructor(
    private shoppingService :ShoppingService,
  @Inject(MAT_DIALOG_DATA) data
  ) {
    this.id = data.id
  }

  ngOnInit(): void {
    this.shoppingService.getCommand(this.id).subscribe(
      (res:CommandedShoppingList)=>{
          this.total = res.total
          this.products = res.products
      }
    )
  }

  passCommand() {
    window.location.href = "http://localhost:4201/devis/"+this.id
  }
}
