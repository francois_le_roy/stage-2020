import {Component, Input, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Recipe} from "../../../../model/selection";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {MatTableDataSource} from "@angular/material/table";
import {SelectionModel} from "@angular/cdk/collections";
import {SearchRecipeService} from "../../../core/services/search-recipe.service";
import {Router} from "@angular/router";
import {SelectionService} from "../../../core/services/selection.service";

@Component({
  selector: 'app-recipe-results',
  templateUrl: './recipe-results.component.html',
  styleUrls: ['./recipe-results.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class RecipeResultsComponent implements OnInit {
  @Input() recipes: Recipe[];
  dataSource: MatTableDataSource<Recipe>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  columnsToDisplay = ['name', 'description','select'];
  expandedElement: Recipe | null;
  selection = new SelectionModel<Recipe>(true, []);
  recipesToAdd: Recipe[] = [];

  constructor(
    private searchRecipeService :SearchRecipeService,
    private selectionService: SelectionService,
    private router:Router
  ) {
  }
  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.recipes);
}
  ngOnChanges(changes: SimpleChanges) {
    this.recipes = changes.recipes.currentValue
    this.dataSource = new MatTableDataSource(this.recipes);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


  recipeOnChange(row: any,e ){
  if(e) {
    this.recipesToAdd.push(row)
  }
  else {
    let index = this.recipesToAdd.indexOf(row)
    this.recipesToAdd.splice(index,1)
  }
  }

  addRecipes() {
      this.searchRecipeService.postRecipe(this.recipesToAdd,this.router.url).subscribe(
        ()=> window.location.reload()
      );
  }
}
