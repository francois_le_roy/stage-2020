import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {SelectionService} from "../../core/services/selection.service";
import {SearchRecipeService} from "../../core/services/search-recipe.service";
import {Recipe} from "../../../model/selection";
import {catchError} from "rxjs/operators";
import {throwError} from "rxjs";

class DialogData {
}

@Component({
  selector: 'app-recipe-search',
  templateUrl: './recipe-search.component.html',
  styleUrls: ['./recipe-search.component.scss']
})
export class RecipeSearchComponent implements OnInit {

  recipeTextForm = new FormGroup({
    text: new FormControl(''),
  });
  recipes: Recipe[]
  isSubmitted: boolean = false;
  isNotFound: boolean;

  constructor(
    readonly searchRecipeService:SearchRecipeService,
    public dialogRef: MatDialogRef<RecipeSearchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
  ngOnInit(): void {
  }
  onSubmit() {
      this.isSubmitted = true
      let textToSearch = this.recipeTextForm.controls['text'].value
        this.searchRecipeService.searchRecipe(textToSearch).subscribe((res: HttpResponse<any>)=>
          {
            catchError((err)=>{
              this.isNotFound=true;
              this.recipes=null
              this.isSubmitted=false
              return throwError(err.message)
            })
            if (res.body.length==0){
              this.isNotFound=true;
              this.recipes=null
              this.isSubmitted=false
            }
            else {
              this.isNotFound=false
              this.recipes=res.body
              this.isSubmitted=false
            }
          })
  }
}


