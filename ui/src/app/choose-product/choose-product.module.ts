import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ChooseProductComponent} from "./choose-product.component";
import {MatDialogModule} from "@angular/material/dialog";
import {MatListModule} from "@angular/material/list";



@NgModule({
  declarations: [
    ChooseProductComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatListModule
  ],
  exports:[
    ChooseProductComponent
  ]
})
export class ChooseProductModule { }
