import {Component, Inject, NgZone, OnInit, ViewChild} from '@angular/core';
import {ShoppingService} from "../core/services/shopping.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DataSource} from "@angular/cdk/collections";
import {MatTableDataSource} from "@angular/material/table";
import {Product, Proposal, Quantity, ShoppingList} from "../../model/selection";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {ActivatedRoute, Router} from "@angular/router";
import {animate, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-store',
  templateUrl: './choose-product.component.html',
  styleUrls: ['./choose-product.component.scss'],
})
export class ChooseProductComponent implements OnInit {
  public quantity;
  public products: Product[]
  public shoppingList: ShoppingList

  constructor(
    private shoppingService:ShoppingService,
    private router: Router,
    public dialogRef: MatDialogRef<ChooseProductComponent>,
  @Inject(MAT_DIALOG_DATA) data
  )
  {
    this.shoppingList = data.shoppingList
    this.quantity= data.quantity;
  }

  ngOnInit(): void {
    let url = this.router.url
    this.shoppingService.listCandidates(url,this.quantity.ingredient.key).subscribe(
      (res:Product[])=>{
        this.products = res;
      }
    )
  }

  addDesiredProduct(product) {
    let url = this.router.url
    this.shoppingService.postCandidate(url,this.quantity.ingredient.key,product).subscribe(
      ()=>{
        this.dialogRef.close()
      }
    )
  }
}
