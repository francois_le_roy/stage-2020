import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppTopBarComponent} from "./app-top-bar.component";
import {MatListModule} from "@angular/material/list";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatToolbarModule} from "@angular/material/toolbar";
import {RouterModule} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";



@NgModule({
  declarations: [
    AppTopBarComponent,
  ],
  imports: [
    CommonModule,
    MatListModule,
    MatButtonToggleModule,
    MatToolbarModule,
    RouterModule,
    MatIconModule,
  ],
  exports: [
    AppTopBarComponent
  ]
})
export class AppTopBarModule {
  constructor() {
  }
}
