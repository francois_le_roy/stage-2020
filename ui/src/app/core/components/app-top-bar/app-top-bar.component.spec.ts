import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppTopBarComponent } from './app-top-bar.component';
import {AuthService} from "../../services/auth.service";

describe('AppTopBarComponent', () => {
  let component: AppTopBarComponent;
  let fixture: ComponentFixture<AppTopBarComponent>;
  let authService;
  let welcomeMsg: HTMLElement;


  class MockAuthService{
    claims = null;
    login(){}
    logout(){}
    get isLoggedIn(){return !!this.claims}
    get name(){return this.claims['given_name']}
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppTopBarComponent],
      providers: [
        AppTopBarComponent,
        {
          provide: AuthService, useClass: MockAuthService
        }
      ]

    })
    component = TestBed.inject(AppTopBarComponent);
    authService = TestBed.inject(AuthService)
    fixture = TestBed.createComponent(AppTopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges()
  }));
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ask for login if if claims are null',  () => {
    let loginButton = fixture.nativeElement.querySelector('mat-button-toggle');
    expect(loginButton.textContent).toEqual("Login")
  });

  it('should ask for logout if claims are not null', function () {
    authService.claims = {given_name : 'bob'};
    fixture.detectChanges()
    let logoutButton = fixture.nativeElement.querySelector('mat-button-toggle');
    expect(logoutButton.textContent).toEqual("Logout")
  });

  it('should render a welcome message if claims are not null', function () {
    authService.claims = {given_name : 'bob'};
    fixture.detectChanges()
    welcomeMsg = fixture.nativeElement.querySelector('.hello-user');
    expect(welcomeMsg.textContent).toEqual("Bienvenue bob")
  });
  it('should call login method on login button click', function () {
    let loginButton = fixture.nativeElement.querySelector('mat-button-toggle');
    spyOn(authService,'login')
    loginButton.click();
    expect(authService.login).toHaveBeenCalled()
  });
  it('should call logout method on logout button click if claims are not null', function () {
    authService.claims = {given_name : 'bob'};
    fixture.detectChanges()
    let logoutButton = fixture.nativeElement.querySelector('mat-button-toggle');
    spyOn(authService,'logout')
    logoutButton.click();
    expect(authService.logout).toHaveBeenCalled()
  });
  it('should have a method isLoggedIn returning true if claims are not null', function () {
    authService.claims = {given_name : 'bob'};
    fixture.detectChanges()
    expect(authService.isLoggedIn).toBeTrue()
  });
  it('should have a method isLoggedIn returning false if claims are  null', function () {
    fixture.detectChanges()
    expect(authService.isLoggedIn).toBeFalse()
  });
});
