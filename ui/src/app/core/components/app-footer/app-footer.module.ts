import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppFooterComponent} from "./app-footer.component";
import {MatToolbarModule} from "@angular/material/toolbar";



@NgModule({
  declarations: [
    AppFooterComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule
  ],
  exports: [
    AppFooterComponent
  ]
})
export class AppFooterModule { }
