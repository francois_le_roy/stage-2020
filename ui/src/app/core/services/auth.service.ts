import { Injectable } from '@angular/core';
import {AuthConfig, JwksValidationHandler, OAuthService} from "angular-oauth2-oidc";
import {CoreModule} from "../core.module";

@Injectable()
export class AuthService {
  constructor(private oauthService: OAuthService, private authConfig: AuthConfig) {
    this.configureOAuth();
  }
  configureOAuth(){
    this.oauthService.configure(this.authConfig)
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
    this.oauthService.setupAutomaticSilentRefresh();
  }
  login(){
    this.oauthService.initImplicitFlow();
  }
  logout(){
    this.oauthService.logOut();
  }
  get claims(){
    return this.oauthService.getIdentityClaims();
  }
  get isLoggedIn(){
    return !!this.claims
  }
  get name(){
    return this.claims ? this.claims['given_name'] : null;
  }
}
