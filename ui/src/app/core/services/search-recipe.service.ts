import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Recipe} from "../../../model/selection";

@Injectable({
  providedIn: 'root'
})
export class SearchRecipeService {

  constructor(
    private http:HttpClient
  ) { }
  searchRecipe(textToSearch){
    const httpOptions = {
      params: new HttpParams().set("textToSearch", textToSearch),
      observe:'response' as const
    }
    return this.http.get<Recipe[]>(environment.recipeUrl+"/search",httpOptions)
  }

  postRecipe(recipe,url) {
    console.log("post "+ environment.selectionUrl+url+"/recipe")
    return this.http.post<Recipe[]>(environment.selectionUrl+url+"/recipe",recipe)
  }
  deleteRecipe(recipe,selectionId,recipeId) {
    return this.http.delete(environment.selectionUrl+"/selection/"+selectionId+"/recipe/"+recipeId,)
  }

}
