import { Injectable } from '@angular/core';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {ChooseStoreComponent} from "../../shared/choose-store/choose-store.component";
import {RecipeSearchComponent} from "../../shared/recipe-search/recipe-search.component";
import {CreateSelectionComponent} from "../../shared/create-selection/create-selection.component";
import {Proposal, Quantity, Selection, ShoppingList} from "../../../model/selection";
import {ChooseProductComponent} from "../../choose-product/choose-product.component";
import {ShoppingComponent} from "../../shopping/shopping-details/shopping.component";
import {CommandComponent} from "../../shared/command/command.component";

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  constructor(
    public dialog: MatDialog
  ) { }
  openRecipeDialog(): void {

    const dialogRef = this.dialog.open(RecipeSearchComponent, {
      width: '500vh',
    })
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openStoreDialog(selection: Selection):void{
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      selection: selection
    };

    const dialogRef = this.dialog.open(ChooseStoreComponent,dialogConfig)
    dialogRef.afterClosed().subscribe(result => {
    });
  };
  openSelectionDialog():void{
    const dialogRef = this.dialog.open(CreateSelectionComponent, {
      width: '500vh',
    })
    dialogRef.afterClosed().subscribe(result => {
    });
  };
  openCandidatesDialog(quantity:Quantity,shoppingList:ShoppingList,component:ShoppingComponent):void{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      quantity: quantity,
      shoppingList:shoppingList
    };
    dialogConfig.width = '50%'
    const dialogRef = this.dialog.open(ChooseProductComponent, dialogConfig)
    dialogRef.afterClosed().subscribe(result => {
      component.ngOnInit()
    });
  }

  openCommandDialog(id:string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      id:id
    }
    dialogConfig.width = '50%'
    const dialogRef = this.dialog.open(CommandComponent, dialogConfig)
    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
