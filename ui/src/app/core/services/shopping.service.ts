import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";
import {Product, Quantity} from "../../../model/selection";

@Injectable({
  providedIn: 'root'
})
export class ShoppingService {

  constructor(private http:HttpClient,private authService:AuthService) {
  }
  getShoppingList(id: string) {
    return this.http.get("http://localhost:8083/shopping/"+id);
  }


  listCandidates(url, ingredient_key) {
    return this.http.get("http://localhost:8083"+url+"/"+ingredient_key)
  }
  postCandidate(url, key, product) {
    return this.http.post("http://localhost:8083"+url+"/"+key,product)
  }
  getShoppingLists() {
    return this.http.get("http://localhost:8083/shopping");
  }
  postSelectionToShop(selectionToConvert){
    return this.http.post("http://localhost:8083/shopping",selectionToConvert,this.httpPostOptions);
  }

  deleteCandidate(quantity: Quantity, id: string) {
    return this.http.delete("http://localhost:8083/shopping/"+id+"/"+quantity.ingredient.key,this.httpDelOptions);
  }
  postListToShop(products:Product[]){
    return this.http.post("http://localhost:8001/lists",products,this.httpPostOptions)
  }
  getCommand(id){
    return this.http.get("http://localhost:8001/lists/"+id)
  }
  get httpPostOptions(){
    return  {
      observe: 'response' as 'response',
      responseType: 'text' as const,
    }
  }
  get httpDelOptions(){
    return  {
      responseType: 'text' as const,
    }
  }


}
