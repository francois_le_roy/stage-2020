import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {AuthService} from "./auth.service";
import {Selection} from "../../../model/selection";
import {tap} from "rxjs/operators";
import {Router} from "@angular/router";



@Injectable({
  providedIn: 'root'
})
export class SelectionService {

  constructor(private http:HttpClient,private authService:AuthService,private router:Router) {

  }
  getSelections(){
    return  this.http.get<Selection[]>(environment.selectionUrl+"/selection",this.httpOptions)
  }
  getSelection(id: string){
    return this.http.get<Selection>(environment.selectionUrl+"/selection/"+id,this.httpOptions)
  }

  postSelection(selectionToPost: string){
    this.http.post(environment.selectionUrl+"/selection", selectionToPost,this.httpPostOptions)
      .subscribe((res:HttpResponse<any>) =>
        {
        let location = res.headers.get("location");
        this.navigateToNewResource(location);
        }
      )
  }
  removeMany(selectionsToRemove:Selection[]) {
    for (let selection of selectionsToRemove){
      this.removeOne(selection).subscribe()
    }
    return this.getSelections()
  }
  removeOne(selection: Selection) {
      return this.http.delete(environment.selectionUrl+"/selection/"+selection.id,this.httpDelOptions);
  }
  navigateToNewResource(location: string){
    this.router.navigateByUrl(location);
  }
  get httpOptions(){
    return  {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
  }
  }
  get httpPostOptions(){
      return  {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
        }),
        //withCredentials: true,
        observe: 'response' as 'response'
    }
}
get httpDelOptions(){
      return  {
        responseType: 'text' as const,
    }
}


}
