import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import {AuthConfig, OAuthService} from "angular-oauth2-oidc";


describe('AuthService', () => {
  let service: AuthService;
  let oAuthService;
  let authConfig;
  let name;


  class MockOAuthService{
    configure(){}
    loadDiscoveryDocumentAndTryLogin(){}
    initImplicitFlow(){}
    logOut(){}
    setupAutomaticSilentRefresh(){}
    getIdentityClaims(){ return {given_name:name}}
  }
  class MockAuthConfig{}
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        AuthService,{
          provide: OAuthService,
          useClass: MockOAuthService
        },
        {
          provide: AuthConfig,
          useClass: MockAuthConfig
        }

      ]});
    service = TestBed.inject(AuthService);
    oAuthService = TestBed.inject(OAuthService)
    authConfig = TestBed.inject(AuthConfig)
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should configure the inits parameters in constructor', function () {
    spyOn(AuthService.prototype,'configureOAuth')
    service = new AuthService(oAuthService,authConfig)
    expect(service.configureOAuth).toHaveBeenCalled();

  });
  it('should call configure method with Authconfig from OAuthService in constructor', function () {
    spyOn(oAuthService,'configure')
    service = new AuthService(oAuthService,authConfig)
    expect(oAuthService.configure).toHaveBeenCalledWith(authConfig)
  });
  it('should call loadDiscoveryDocumentAndTryLogin method from OAuthService in constructor', function () {
    spyOn(oAuthService,'loadDiscoveryDocumentAndTryLogin')
    service = new AuthService(oAuthService,authConfig)
    expect(oAuthService.loadDiscoveryDocumentAndTryLogin).toHaveBeenCalled()
  });
  it('should call initImplicitFlow method from OAuthService when login method is called', function () {
    spyOn(oAuthService,'initImplicitFlow')
    service.login()
    expect(oAuthService.initImplicitFlow).toHaveBeenCalled()
  });
  it('should call logOut method from OAuthservice when logout method is called', function () {
    spyOn(oAuthService,'logOut')
    service.logout()
    expect(oAuthService.logOut).toHaveBeenCalled()
  });
  it('should render given_name as name', function () {
    name='bob'
    service = new AuthService(oAuthService,authConfig)
    expect(service.name).toEqual("bob")
  });
  it('should render given_name equals to null if name is not defined', function () {
    name = null
    service = new AuthService(oAuthService,authConfig)
    expect(service.name).toEqual(null)
  });
  it('should have a method isLoggedIn returning true if claims are not null', function () {
    name='bob'
    service = new AuthService(oAuthService,authConfig)
    expect(service.isLoggedIn).toBeTrue()
  });
});
