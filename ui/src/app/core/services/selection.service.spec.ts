import { TestBed } from '@angular/core/testing';

import { SelectionService } from './selection.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {AuthService} from "./auth.service";

describe('SelectionService', () => {
  let service: SelectionService;
  let httpTestingController: HttpTestingController;
  let authService:AuthService

  class MockAuthService{

  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers:[SelectionService,
        {
          provide:AuthService,useClass:MockAuthService
        }
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    authService = TestBed.inject(AuthService);
    service = TestBed.inject(SelectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
