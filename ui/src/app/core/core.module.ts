import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthService} from "./services/auth.service";
import {AuthConfig} from "angular-oauth2-oidc";
import {auth} from "./auth";
import {AppTopBarModule} from "./components/app-top-bar/app-top-bar.module";
import {AppFooterModule} from "./components/app-footer/app-footer.module";
import {DialogService} from "./services/dialog.service";
import {SearchRecipeService} from "./services/search-recipe.service";
import {SelectionService} from "./services/selection.service";
import {ShoppingService} from "./services/shopping.service";



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [
    AuthService,{
      provide:AuthConfig,
      useValue:auth
    },
    DialogService,
    SearchRecipeService,
    SelectionService,
    ShoppingService
  ],
  exports: [
    AppTopBarModule,
    AppFooterModule,
  ],
})
export class CoreModule { }
