import { NgModule } from '@angular/core';
import { Routes, RouterModule,CanActivate } from '@angular/router';
import {HomepageComponent} from "./homepage/connexion-home/homepage.component";
import {CreateSelectionComponent} from "./shared/create-selection/create-selection.component";
import {DisplaySelectionComponent} from "./selection/display-selection/display-selection.component";
import {AuthGuardService as AuthGuard} from "./auth-guard.service";
import {SelectionComponent} from "./selection/selection-details/selection.component";
import {RecipeSearchComponent} from "./shared/recipe-search/recipe-search.component";
import {ShoppingComponent} from "./shopping/shopping-details/shopping.component";
import {DisplayShoppingListComponent} from "./shopping/display-shopping-list/display-shopping-list.component";

const routes: Routes = [
  { path : '', component: HomepageComponent},
  { path: 'create-selection', component: CreateSelectionComponent,canActivate: [AuthGuard]},
  { path: 'display-selection', component: DisplaySelectionComponent,canActivate: [AuthGuard]},
  { path: 'selection/:id',component:SelectionComponent,canActivate:[AuthGuard]},
  { path: 'shopping/:id',component:ShoppingComponent,canActivate:[AuthGuard]},
  { path: 'display-list',component:DisplayShoppingListComponent,canActivate:[AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
