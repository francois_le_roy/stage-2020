class Ingredient {
  key: string;
  name: string
}

class Unit {
  key: string;
  name: string
}

export class Quantity {
  ingredient:Ingredient;
  unit: Unit;
  quantity: number
}

export interface Recipe {
id: string;
name: string;
description:string;
quantities: Quantity[]
}
export interface Selection {
  id: string;
  name: string;
  recipes: Recipe[];
}
export class SelectionAndStore {
  store: string;
  selection:Selection;
}

class Measure {
}

class Item {
}

class Shopping {
  item : Item;
  quantity: Measure;
}

export class Product {
  id :String;
  product_name:String;
  code:String;
  categories:String[]
  quantity:String
  stores_tags:String[]
  count: number
}

export class Proposal {
  quantity:Quantity;
  product:Product;
}

export interface ShoppingList {
  id : string;
  name: string;
  shoppingList:Shopping[];
  baseList:Quantity[];
  proposals: Proposal[]
  store:string;
}
export interface CommandedShoppingList {
  id: String;
  products: CommandedProduct[];
  total: number;
}
export class CommandedProduct {
  id :String;
  product_name:String;
  code:String;
  price:String
  quantity:String
  count: number

}
