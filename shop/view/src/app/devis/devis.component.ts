import {Component, OnInit, ViewChild} from '@angular/core';
import {RequestService} from "../request.service";
import {Delivery, Product, ShoppingList} from "../../model/model";
import {ActivatedRoute} from "@angular/router";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-devis',
  templateUrl: './devis.component.html',
  styleUrls: ['./devis.component.scss']
})
export class DevisComponent implements OnInit {
  shoppingList: ShoppingList
  displayedColumns: string[] = ['product_name', 'price', 'quantity','count'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  chooseDeliveryType: FormGroup;
  id = this.route.snapshot.paramMap.get('id')

  type = new FormControl('type');
  storefordrive= new FormControl('storefordrive');
  choosePaymentMeans: FormGroup;
  means = new FormControl('type');
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource: MatTableDataSource<Product>;
  constructor(
    private requestService:RequestService,
    private route:ActivatedRoute,
    fb: FormBuilder
  ) {
    this.chooseDeliveryType = fb.group({
      type: this.type,
      storefordrive:this.storefordrive,
      address:new FormControl('')
    });
    this.choosePaymentMeans = fb.group({
      means: this.means,
    });
  }
  ngOnInit(): void {
    this.refreshData()
  }

  onSubmit() {
    let delivery = new Delivery();
    delivery.type = this.type.value;
    if(this.type.value == "Drive"){
      delivery.address = this.storefordrive.value
    }
    if(this.type.value == "Livraison"){
      console.log(this.chooseDeliveryType.get("address"))
      delivery.address = this.chooseDeliveryType.get("address").value
    }
    this.requestService.postDeliveryType(this.id,delivery).subscribe();
    this.refreshData()
  }

  onSubmitPay() {
    this.requestService.postPaymentMeans(this.id,this.means.value).subscribe();
    this.refreshData()
  }
  refreshData(){

    this.requestService.getShoppingListWithId(this.id).subscribe(
      (res:ShoppingList)=>
      {
        this.shoppingList = res
        this.dataSource = new MatTableDataSource<Product>(this.shoppingList.products);
        this.dataSource.paginator = this.paginator;
      }
    )
  }
}
