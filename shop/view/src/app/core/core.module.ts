import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from './top-bar/top-bar.component';
import { FooterComponent } from './footer/footer.component';
import {MatToolbarModule} from "@angular/material/toolbar";



@NgModule({
  declarations: [TopBarComponent, FooterComponent],
  imports: [
    CommonModule,
    MatToolbarModule
  ],
  exports: [TopBarComponent,FooterComponent]
})
export class CoreModule { }
