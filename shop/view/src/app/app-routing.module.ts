import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';
import {DevisComponent} from './devis/devis.component';

const routes: Routes = [
  { path : '', component: AppComponent},
  { path: 'devis/:id', component: DevisComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

