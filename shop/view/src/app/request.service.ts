import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private httpClient: HttpClient ) { }
  getShoppingListWithId(id){
    return this.httpClient.get("http://localhost:8001/lists/"+id,this.httpOptions)
}
  get httpOptions(){
    return  {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
    }
  }

  postDeliveryType(id,value) {
    return this.httpClient.post("http://localhost:8001/lists/"+id+"/delivery",value,this.httpPostOptions)
  }
  postPaymentMeans(id,value) {
    return this.httpClient.post("http://localhost:8001/lists/"+id+"/payment",value,this.httpPostOptions)
  }
  get httpPostOptions(){
    return  {
      observe: 'response' as 'response',
      responseType: 'text' as const,
    }
  }
}
