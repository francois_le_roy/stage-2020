export class Product {
  id :String;
  product_name:String;
  code:String;
  price:String
  quantity:String
  count:number;
}
export class ShoppingList {
  id: String;
  products: Product[];
  total: number;
  payment_means: String;
  delivery_type: Delivery;
}
export class Delivery {
  type:String;
  address:String;
}
