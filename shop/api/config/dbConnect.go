package config

import (
  "context"
  "fmt"
  "go.mongodb.org/mongo-driver/mongo"
  "go.mongodb.org/mongo-driver/mongo/options"
  "log"
)

func ConnectProductCollection() *mongo.Collection {
  // Set client options
  clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
  // Connect to MongoDB
  client, err := mongo.Connect(context.TODO(), clientOptions)
  if err != nil {
    log.Fatal(err)
  }
  // Check the connection
  err = client.Ping(context.TODO(), nil)
  if err != nil {
    log.Fatal(err)
  }
  fmt.Println("Connected to MongoDB collection shop!")
  shop := client.Database("test_db").Collection("shop")
  return shop
}

func ConnectShoppingListCollection() *mongo.Collection {
  // Set client options
  clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
  // Connect to MongoDB
  client, err := mongo.Connect(context.TODO(), clientOptions)
  if err != nil {
    log.Fatal(err)
  }
  // Check the connection
  err = client.Ping(context.TODO(), nil)
  if err != nil {
    log.Fatal(err)
  }
  fmt.Println("Connected to MongoDB collection lists!")
  shop := client.Database("test_db").Collection("lists")
  return shop
}
