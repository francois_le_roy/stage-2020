package service

import (
  "../config"
  "../model"
  "../repository"
  "github.com/julienschmidt/httprouter"
  "net/http"
)

type ProductService struct{
  productRepository repository.ProductRepository
}
func NewProductService() ProductService {
  productRepository := repository.ProductRepository{
    Shop: config.ConnectProductCollection(),
  }
  service := ProductService{productRepository}
  return service
}
func (service ProductService) GetAll() model.Products {
  return service.productRepository.FindAllProduct()
}

func (service ProductService)  GetOne(id string) model.Product{
  return service.productRepository.FindProductById(id)
}
func  (service ProductService) PostOne(product model.Product) string{
  if service.productRepository.FindProductById(product.Id).Id== "" {
    service.productRepository.InsertProduct(product)
  } else {
    service.productRepository.UpdateProduct(product)
  }
  return product.Id
}
func (service ProductService)  PostAShoppingList(writer http.ResponseWriter, request *http.Request, ps httprouter.Params) {
}
