package service

import (
  "../config"
  "../model"
  "../repository"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "math"
)

type ShoppingListService struct{
  shoppingListRepository repository.ShoppingListRepository
  productRepository repository.ProductRepository
  }
func NewShoppingListService() ShoppingListService {
  shoppingListRepository := repository.ShoppingListRepository{
    Shop: config.ConnectShoppingListCollection(),
  }
  productRepository := repository.ProductRepository{
    Shop: config.ConnectProductCollection(),
  }
  service := ShoppingListService{shoppingListRepository,productRepository}
  return service
}

func (service ShoppingListService) CreateOne(products model.Products) string {
  var sum = 0.00
  productsFromShop := model.Products{}
  for _,product := range products {
    productFromShop := service.productRepository.FindProductByCode(product.Code)
    if productFromShop.Code=="" {
      sum+=0.0
      productsFromShop = append(productsFromShop,model.Product{
        Id:          product.Id,
        ProductName: product.ProductName,
        Price:       0,
        Code:        "",
        Quantity:    "",
        Count:       0,
      })
    } else{
      sum += productFromShop.Price * float64(product.Count)
      productFromShop.Count = product.Count
      productsFromShop = append(productsFromShop, productFromShop)
    }


  }
  shoppingList := model.ShoppingList{
    Id:           primitive.NewObjectID(),
    Products:     productsFromShop,
    Total:        math.Floor(sum*100)/100,
    PaymentMeans: "",
    DeliveryType: model.Delivery{},
  }
  service.shoppingListRepository.InsertShoppingList(shoppingList)
  return shoppingList.Id.Hex()
}
func (service ShoppingListService) GetAllShoppingList() model.ShoppingLists{
  return service.shoppingListRepository.FindAllShoppingList()
}

func (service ShoppingListService) GetOneList(id string) model.ShoppingList {
  return service.shoppingListRepository.FindShoppingListById(id)
}

func (service ShoppingListService) UpdateDeliveryType(id string, deliveryType model.Delivery) string {
  var list = service.GetOneList(id)
  list.DeliveryType = deliveryType
  service.shoppingListRepository.UpdateShoppingList(list)
  return id
}

func (service ShoppingListService) UpdatePaymentMeans(id string, paymentMeans string)  string {
  var list = service.GetOneList(id)
  list.PaymentMeans = paymentMeans
  service.shoppingListRepository.UpdateShoppingList(list)
  return id
}

