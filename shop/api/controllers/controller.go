package controllers

import (
  "../model"
  "../service"
  "encoding/json"
  "fmt"
  "github.com/julienschmidt/httprouter"
  "io/ioutil"
  "log"
  "net/http"
)
type Controller struct{
  productService service.ProductService
  shoppingListService service.ShoppingListService
}
func New() Controller{
  productService := service.NewProductService()
  shoppingListService := service.NewShoppingListService()
  controller := Controller{productService,shoppingListService}
  return controller
}
func (controller Controller) GetAll(writer http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  writer.Header().Set("Access-Control-Allow-Origin","*")
  writer.Header().Set("content-type", "application/json")
  err := json.NewEncoder(writer).Encode(controller.productService.GetAll())
  if err!=nil {
    fmt.Println(err.Error())
  }
}

func (controller Controller)  GetOne(writer http.ResponseWriter, request *http.Request, ps httprouter.Params) {
  writer.Header().Set("Access-Control-Allow-Origin","*")
  writer.Header().Set("content-type", "application/json")
  json.NewEncoder(writer).Encode(controller.productService.GetOne( ps.ByName("id")))

}
func (controller Controller) PostOne(writer http.ResponseWriter, request *http.Request, ps httprouter.Params){
  var product model.Product
  err := json.NewDecoder(request.Body).Decode(&product)
  if err!=nil{
    http.Error(writer,err.Error(),http.StatusBadRequest)
    return
  }
  id := controller.productService.PostOne(product)
  writer.Header().Set("Access-Control-Allow-Origin","*")
  writer.WriteHeader(http.StatusCreated)
  writer.Write([]byte("products/"+id))
}
func (controller Controller)  CreateShoppingList(writer http.ResponseWriter, request *http.Request, ps httprouter.Params) {
  var products model.Products
  err := json.NewDecoder(request.Body).Decode(&products)
  if err!=nil{
    http.Error(writer,err.Error(),http.StatusBadRequest)
    return
  }
  id := controller.shoppingListService.CreateOne(products)
  writer.Header().Set("Access-Control-Allow-Origin","*")
  writer.WriteHeader(http.StatusCreated)
  writer.Write([]byte(id))
}
func (controller Controller) GetAllShoppingList(writer http.ResponseWriter, request *http.Request, ps httprouter.Params) {
  writer.Header().Set("content-type", "application/json")
  writer.Header().Set("Access-Control-Allow-Origin","*")

  json.NewEncoder(writer).Encode(controller.shoppingListService.GetAllShoppingList())
}
func (controller Controller) GetShoppingList(writer http.ResponseWriter, request *http.Request, ps httprouter.Params) {
  writer.Header().Set("content-type", "application/json")
  writer.Header().Set("Access-Control-Allow-Origin","*")
  json.NewEncoder(writer).Encode(controller.shoppingListService.GetOneList( ps.ByName("id")))
}

func (controller Controller) UpdateDeliveryType(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
  writer.Header().Set("Access-Control-Allow-Origin","*")
  var deliveryType model.Delivery
  err := json.NewDecoder(request.Body).Decode(&deliveryType)
  if err!=nil{
    http.Error(writer,err.Error(),http.StatusBadRequest)
    return
  }
  json.NewEncoder(writer).Encode(controller.shoppingListService.UpdateDeliveryType( params.ByName("id"),deliveryType))
}
func (controller Controller) UpdatePaymentMeans(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
  writer.Header().Set("Access-Control-Allow-Origin","*")
  var paymentMeans string
  responseData,err := ioutil.ReadAll(request.Body)
  if err != nil {
    log.Fatal(err)
  }
  paymentMeans = string(responseData)
  json.NewEncoder(writer).Encode(controller.shoppingListService.UpdatePaymentMeans( params.ByName("id"),paymentMeans))
}
