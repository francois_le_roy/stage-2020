package repository

import (
  "../model"
  "context"
  "fmt"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/mongo"
  "os"
)


type ProductRepository struct {
  Shop *mongo.Collection
}

func (shop ProductRepository) FindProductById(id string) model.Product {
  var product model.Product
  err := shop.Shop.FindOne(context.TODO(), bson.M{"_id": "5400101013018"}).Decode(&product)
  if err != nil {
    fmt.Println(err)
  }
  return product
}
func (shop ProductRepository) FindProductByCode(code string) model.Product {
  var product model.Product
  err := shop.Shop.FindOne(context.TODO(), bson.M{"code":code}).Decode(&product)
  if err != nil {
    return model.Product{}
    fmt.Println(err)
  }
  return product
}

func (shop ProductRepository) FindAllProduct() model.Products {
  var products model.Products
  cursor, err := shop.Shop.Find(context.TODO(), bson.D{} )
  if err != nil {
    fmt.Println(err)
  }
  for cursor.Next(context.TODO()) {
    var product model.Product
    err := cursor.Decode(&product)
    if err != nil {
      fmt.Println("cursor.Next() error:", err)
      os.Exit(1)
      // If there are no cursor.Decode errors
    } else {
      products = append(products,product)
    }
  }
  return products
}

func (shop ProductRepository) UpdateProduct (product model.Product)  {
  id := product.Id
  shop.Shop.ReplaceOne(context.TODO(),bson.M{"_id":id},bson.M{
    "_id": product.Id,
    "product_name":product.ProductName,
    "code":product.Code,
    "price":product.Price,
  })
}
func (shop ProductRepository) InsertProduct(product model.Product)  {
  shop.Shop.InsertOne(context.TODO(),product)
}

