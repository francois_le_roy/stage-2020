package repository

import (
  "../model"
  "context"
  "fmt"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "go.mongodb.org/mongo-driver/mongo"
  "os"
)


type ShoppingListRepository struct {
  Shop *mongo.Collection
}

func (shop ShoppingListRepository) FindShoppingListById(id string) model.ShoppingList {
  var shoppingList model.ShoppingList
  oid, err := primitive.ObjectIDFromHex(id)
  if err != nil {
    fmt.Println(err)
  }
  err = shop.Shop.FindOne(context.TODO(), bson.M{"_id": oid}).Decode(&shoppingList)
  if err != nil {
    fmt.Println(err)
  }
  return shoppingList
}

func (shop ShoppingListRepository) FindAllShoppingList() model.ShoppingLists{
  var ShoppingLists model.ShoppingLists
  cursor, err := shop.Shop.Find(context.TODO(), bson.D{} )
  if err != nil {
    fmt.Println(err)
  }
  for cursor.Next(context.TODO()) {
    var ShoppingList model.ShoppingList
    err := cursor.Decode(&ShoppingList)
    if err != nil {
      fmt.Println("cursor.Next() error:", err)
      os.Exit(1)
      // If there are no cursor.Decode errors
    } else {
      ShoppingLists = append(ShoppingLists,ShoppingList)
    }
  }
  return ShoppingLists
}

func (shop ShoppingListRepository) UpdateShoppingList (ShoppingList model.ShoppingList)  {
  id := ShoppingList.Id
  shop.Shop.ReplaceOne(context.TODO(),bson.M{"_id":id},bson.M{
    "_id": ShoppingList.Id,
    "total":ShoppingList.Total,
    "products":ShoppingList.Products,
    "delivery_type":ShoppingList.DeliveryType,
    "payment_means":ShoppingList.PaymentMeans,
  })
}
func (shop ShoppingListRepository) InsertShoppingList(ShoppingList model.ShoppingList)  {
  shop.Shop.InsertOne(context.TODO(),ShoppingList)
}
