package main

import (
"./controllers"
"github.com/julienschmidt/httprouter"
"log"
"net/http"
)

func main() {
  //server := &http.Server{Addr: ":8080", Handler: handler}
  router := httprouter.New()
  router.GlobalOPTIONS = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    if r.Header.Get("Access-Control-Request-Method") != "" {
      header := w.Header()
      header.Set("Access-Control-Allow-Headers", "content-type")
      header.Set("Access-Control-Allow-Methods", header.Get("Allow"))
      header.Set("Access-Control-Allow-Origin", "*")
      header.Set("content-type", "application/json")
    }

    // Adjust status code to 204
    //w.WriteHeader(http.StatusNoContent)
  })
  controller := controllers.New()
  router.GET("/products",controller.GetAll)
  router.GET("/products/:id",controller.GetOne)
  router.POST("/products",controller.PostOne)
  router.POST("/lists",controller.CreateShoppingList)
  router.GET("/lists",controller.GetAllShoppingList)
  router.GET("/lists/:id",controller.GetShoppingList)
  router.POST("/lists/:id/delivery",controller.UpdateDeliveryType)
  router.POST("/lists/:id/payment",controller.UpdatePaymentMeans)
  log.Fatal(http.ListenAndServe(":8001", router))

}

