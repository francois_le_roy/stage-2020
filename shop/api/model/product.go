package model

type Product struct {
  Id          string `json:"_id" bson:"_id"`
  ProductName string `json:"product_name" bson:"product_name"`
  Price       float64    `json:"price" bson:"price"`
  Code        string `json:"code" bson:"code"`
  Quantity    string `json:"quantity" bson:"quantity"`
  Count       int `json:"count" bson:"count"`
}

type Products []Product
