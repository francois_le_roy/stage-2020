package model

import (
  "go.mongodb.org/mongo-driver/bson/primitive"
)

type Delivery struct{
  Type string  `json:"type" bson:"type"`
  Address string  `json:"address" bson:"address"`
}

type ShoppingList struct {
  Id           primitive.ObjectID   `json:"_id" bson:"_id"`
  Products     Products `json:"products" bson:"products"`
  Total        float64      `json:"total" bson:"total"`
  PaymentMeans string   `json:"payment_means" bson:"payment_means"`
  DeliveryType Delivery   `json:"delivery_type" bson:"delivery_type"`
}
type ShoppingLists []ShoppingList
