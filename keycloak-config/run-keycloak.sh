#!/bin/bash

sudo docker rm kc-import

sudo docker run --name kc-import -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_IMPORT=/tmp/keycloak-config.json -p 8080:8080 -v /home/francois/IdeaProjects/stage/keycloak-config/keycloak-config.json:/tmp/keycloak-config.json quay.io/keycloak/keycloak:10.0.2
