const superagent = require('superagent');

let oauthOptions = {
    url: 'https://api.orange.com/oauth/v2/token',
    form: {
        grant_type: "client_credentials",
        client_id: "CkeChDbeqjuEZF2Kxlr9aDBocou5ziVf",
        client_secret: "FoKnijXRAd6g4zPH",
        redirect_uri: "http://localhost:8082"
    }

}

searcher = function(arg, rep) {
    let argEncoded = encodeURIComponent(arg);
    console.log(argEncoded)
    superagent
        .post(oauthOptions.url)
        .send(oauthOptions.form)
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .then(getAccessToken)
        .then(getRecipesMatches)
        .catch(err => {console.log(err)})

    function getAccessToken(res) {
        token = res.body.access_token;
        return token
    }
    function getRecipesMatches(token){
            superagent
                .get('https://api.orange.com/food/v1/search/recipes?text='+argEncoded+'&audiences=PUBLIC')
                .set( 'Accept', 'application/hal+json')
                .set('Authorization', 'Bearer '+token)
                .end((err, res) => {
                    if(err){
                        console.error(err);
                    }
                    if(res.body._embedded!=null){
                      try {rep.send(res.body._embedded.recipes)}
                      catch (e) {
                        console.log(e)
                      }
                    }
                    else {
                      rep.send("[]");
                    }
                    rep.end()
                });
    }
}
module.exports.searcher = searcher
