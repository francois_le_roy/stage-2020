let session = require('express-session');
let Keycloak = require('keycloak-connect');
let cors = require('cors')
let express = require('express');
let bodyParser = require('body-parser')

let app = express()

app.use(cors({
  origin: 'http://localhost:4200'
}))
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
app.use(bodyParser.json())

var memoryStore = new session.MemoryStore();

app.use(session({
  secret: 'some secret',
  resave: false,
  saveUninitialized: true,
  store: memoryStore
}));

var keycloak = new Keycloak({ store: memoryStore });

app.use( keycloak.middleware() );

let textSearchRecipe = require("./lib/handler/textRecipeSearcher.js")

app.get('/', keycloak.protect(),function (req,res) {
  res.setHeader('Content-Type','text/html');
  res.status(200).send('<h1>Formulaire de test</h1><form action="/search" method="get"><input type="text" name="textToSearch"><input type="submit"></form>');
})
app.get('/search',keycloak.protect(),function (req,res) {
  textSearchRecipe.searcher(req.query.textToSearch,res)

})
//Launch server
app.listen('8082','localhost', err => {
  if (err) {
    console.error(err);
  }
  {
    console.log(`APP Listen to port : 8082`);
  }
});
