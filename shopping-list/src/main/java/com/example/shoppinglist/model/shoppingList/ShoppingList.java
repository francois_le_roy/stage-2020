package com.example.shoppinglist.model.shoppingList;

import com.example.shoppinglist.model.selection.Quantity;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document
public class ShoppingList {
  @Id
  private String id;
  private String name;
  private List<Shopping> shoppingList;
  private List<Quantity> baseList;
  private List<Proposal> proposals;
  private String store;
}
