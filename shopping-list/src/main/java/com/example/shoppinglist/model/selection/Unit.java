package com.example.shoppinglist.model.selection;

import lombok.Data;

@Data
public class Unit {
  private String key;
  private String name;
}
