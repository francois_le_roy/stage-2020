package com.example.shoppinglist.model.shoppingList;

import com.example.shoppinglist.model.off.Product;
import lombok.Data;

import java.util.List;

@Data
public class Shopping {
  private Item item;
  private Measure quantity;
}
