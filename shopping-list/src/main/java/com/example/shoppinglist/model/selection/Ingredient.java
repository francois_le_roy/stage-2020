package com.example.shoppinglist.model.selection;

import lombok.Data;

@Data
public class Ingredient {
  private String key;
  private String name;
}
