package com.example.shoppinglist.model.transition;

import com.example.shoppinglist.model.selection.Selection;
import com.example.shoppinglist.model.shoppingList.Store;
import lombok.Data;

@Data
public class SelectionToConvert {
  private Selection selection;
  private String store;
}
