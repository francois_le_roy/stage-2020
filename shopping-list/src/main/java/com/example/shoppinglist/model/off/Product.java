package com.example.shoppinglist.model.off;

import com.example.shoppinglist.model.selection.Quantity;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
public class Product{
  @Id
  private String  id;
  //@Field
  //private String ingredients_text_fr;
  @Field
  private String product_name;
  @Field
  private String generic_name;
  @Field
  private String code;
  @Field
  private String url;
  @Field
  private String[] categories;
  @Field
  private String quantity;
  @Field
  private String[] stores_tags;
  @Override
  public boolean equals(Object o){
    if(o instanceof Product){
      return ((Product) o).getId().equals(this.id);
    }
    return false;
  }
}
