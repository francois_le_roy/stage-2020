package com.example.shoppinglist.model.selection;

import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class Quantity {
  private Ingredient ingredient;
  private Unit unit;
  private int quantity;
  @Override
  public boolean equals(Object o){
    if(o instanceof Quantity){
      return ((Quantity) o).getIngredient().getKey().equals(this.ingredient.getKey());
    }
    return false;
  }

  public void update(int quantity,Unit unit) {
    if(this.unit.equals(unit)){
      this.quantity+=quantity;
    }
  }
}
