package com.example.shoppinglist.model.selection;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@Document
public class Recipe {
    @Id
    private String id;
    @Field
    private String name;
    @Field
    private String description;
    @Field
    private List<Quantity> quantities;
}
