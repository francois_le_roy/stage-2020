package com.example.shoppinglist.model.shoppingList;

import com.example.shoppinglist.model.off.Product;
import com.example.shoppinglist.model.selection.Quantity;
import lombok.Data;

import java.util.List;

@Data
public class Proposal {
  private Quantity quantity;
  private Product product;
}
