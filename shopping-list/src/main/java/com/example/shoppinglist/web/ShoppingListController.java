package com.example.shoppinglist.web;

import com.example.shoppinglist.model.off.Product;
import com.example.shoppinglist.model.selection.Quantity;
import com.example.shoppinglist.model.shoppingList.ShoppingList;
import com.example.shoppinglist.model.transition.SelectionToConvert;
import com.example.shoppinglist.service.ShoppingListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Pair;
import org.springframework.expression.spel.ast.Selection;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ShoppingListController {
  private static final Logger logger = LoggerFactory.getLogger(ShoppingListController.class);
  private final ShoppingListService shoppingListService;

  public ShoppingListController(ShoppingListService shoppingListService) {
    this.shoppingListService = shoppingListService;
  }
  @PostMapping("/shopping")
  public ResponseEntity postSelectionAndStore(@RequestBody SelectionToConvert selectionToConvert) throws Exception {
    logger.info("create a shopping list");
    ShoppingList postedShoppingList = shoppingListService.createShoppingList(selectionToConvert);
    URI location = URI.create(String.format("/shopping/%s", postedShoppingList.getId()));
    return ResponseEntity.created(location).build();
  }
  @GetMapping("shopping/{id}")
  public ResponseEntity getShoppingList(@PathVariable String id){
    logger.info("get a shopping list");
    return ResponseEntity.ok(shoppingListService.findShoppingList(id));
  }
  @GetMapping("shopping")
  public ResponseEntity getShoppingsList(){
    logger.info("get all shopping list");
    return ResponseEntity.ok(shoppingListService.findAllShoppingList());
  }
  @GetMapping("shopping/{id}/{ingredient_key}")
  public ResponseEntity<List<Product>> listCandidates(@PathVariable String id, @PathVariable String ingredient_key ){
    logger.info("get next candidates");
    return ResponseEntity.ok(shoppingListService.listCandidates(id,ingredient_key));
  }
  @PostMapping("shopping/{id}/{ingredient_key}")
  public ResponseEntity<ShoppingList> postCandidate(@PathVariable String id, @PathVariable String ingredient_key,@RequestBody Product product){
    logger.info("post new candidates");
    ShoppingList modifiedShoppingList =shoppingListService.postCandidate(id,ingredient_key,product);
    URI location = URI.create(String.format("/shopping/%s", modifiedShoppingList.getId()));
    return ResponseEntity.created(location).build();
  }
  @DeleteMapping("shopping/{id}/{ingredient_key}")
  public ResponseEntity deleteCandidate(@PathVariable String id, @PathVariable String ingredient_key){
    logger.info("delete candidate");
    boolean hasBeenDeleted = shoppingListService.deleteCandidate(id,ingredient_key);
    return ResponseEntity.ok(id);
  }
}
