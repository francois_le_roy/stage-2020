package com.example.shoppinglist.utils;

import com.example.shoppinglist.model.selection.Quantity;
import com.example.shoppinglist.model.shoppingList.Shopping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CandidateChooser {
  List<Shopping> choose(List<Quantity> baseList);
}
