package com.example.shoppinglist.repository;

import com.example.shoppinglist.model.shoppingList.ShoppingList;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShoppingRepository extends MongoRepository<ShoppingList,String> {

}
