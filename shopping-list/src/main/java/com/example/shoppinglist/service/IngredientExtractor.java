package com.example.shoppinglist.service;

import com.example.shoppinglist.model.selection.Quantity;
import com.example.shoppinglist.model.selection.Recipe;
import com.example.shoppinglist.model.selection.Selection;

import java.util.ArrayList;
import java.util.List;

public class IngredientExtractor {
  public List<Quantity> extract(Selection selection){
    List<Quantity> baseList = new ArrayList<>();
    List<Recipe> toExtractQuantity = selection.getRecipes();
    for (Recipe recipe: toExtractQuantity){
      for(Quantity quantity: recipe.getQuantities()){
        if(baseList.contains(quantity)){
          int index = baseList.indexOf(quantity);
          Quantity quantityToUpdate = baseList.get(index);
          quantityToUpdate.update(quantity.getQuantity(),quantity.getUnit());
        }
        else {
          if (quantity.getQuantity()!=0){
            baseList.add(quantity);
          }
        }
      }
    }
    return baseList;
  }
}
