package com.example.shoppinglist.service;

import com.example.shoppinglist.model.off.Product;
import com.example.shoppinglist.model.selection.Ingredient;
import com.example.shoppinglist.model.selection.Quantity;
import com.example.shoppinglist.model.shoppingList.Item;
import com.example.shoppinglist.model.shoppingList.Proposal;
import com.example.shoppinglist.model.shoppingList.ShoppingList;
import com.example.shoppinglist.model.transition.SelectionToConvert;
import com.example.shoppinglist.repository.ShoppingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.time.LocalDate.*;
import java.util.Map;

@Service
@Slf4j
public class ShoppingListService {
  ProductPicker productPicker;
  IngredientExtractor ingredientExtractor;
  ShoppingRepository shoppingRepository;

  public ShoppingListService(ProductPicker productPicker, ShoppingRepository shoppingRepository) {
    this.productPicker = productPicker;
    this.ingredientExtractor = new IngredientExtractor();
    this.shoppingRepository = shoppingRepository;
  }

  public ShoppingList createShoppingList(SelectionToConvert selectionToConvert) throws Exception {
    ShoppingList shoppingList = new ShoppingList();
    String store = selectionToConvert.getStore();
    shoppingList.setName(selectionToConvert.getSelection().getName() + " du " + getActualDate());
    shoppingList.setStore(store);
    List<Quantity> baseList = ingredientExtractor.extract(selectionToConvert.getSelection());
    shoppingList.setBaseList(baseList);
    List<Proposal> proposition = productPicker.pick(baseList, store, 3);
    shoppingList.setProposals(proposition);
    return shoppingRepository.save(shoppingList);
  }

  public ShoppingList findShoppingList(String id) {
    return shoppingRepository.findById(id).get();
  }

  public String getActualDate() throws ParseException {
    return LocalDate.now().toString();
  }

  public List<ShoppingList> findAllShoppingList() {
    return shoppingRepository.findAll();
  }

  public List<Product> listCandidates(String id, String ingredient_key) {
    ShoppingList shoppingList = shoppingRepository.findById(id).get();
    Quantity toFind = new Quantity();
    Ingredient toFindIng = new Ingredient();
    toFindIng.setKey(ingredient_key);
    toFind.setIngredient(toFindIng);
    int index = shoppingList.getBaseList().indexOf(toFind);
    Quantity quantityToMatch = shoppingList.getBaseList().get(index);
    String store = shoppingList.getStore();
    List<Product> candidates = productPicker.getCandidates(quantityToMatch, store, 10);
    return candidates;
  }

  public ShoppingList postCandidate(String id, String ingredient_key, Product product) {
    ShoppingList shoppingList = this.shoppingRepository.findById(id).get();
    List<Proposal> proposals = shoppingList.getProposals();
    Proposal toUpdate = findProposalWithKey(proposals,ingredient_key);
    toUpdate.setProduct(product);
    return this.shoppingRepository.save(shoppingList);
  }

  public boolean deleteCandidate(String id, String ingredient_key) {
    ShoppingList shoppingList = this.shoppingRepository.findById(id).get();
    List<Proposal> proposals = shoppingList.getProposals();
    Proposal toDelete = findProposalWithKey(proposals, ingredient_key);
    shoppingList.getBaseList().remove(toDelete.getQuantity());
    shoppingList.getProposals().remove(toDelete);
    this.shoppingRepository.save(shoppingList);
    return true;
  }

  public Proposal findProposalWithKey(List<Proposal> proposals, String ingredient_key) {
    Quantity toFind = new Quantity();
    Proposal toDelete = null;
    Ingredient mockIng = new Ingredient();
    mockIng.setKey(ingredient_key);
    toFind.setIngredient(mockIng);
    for (Proposal proposal : proposals) {
      if (proposal.getQuantity().equals(toFind)) {
        return proposal;
      }
    }
    return null;
  }
}
