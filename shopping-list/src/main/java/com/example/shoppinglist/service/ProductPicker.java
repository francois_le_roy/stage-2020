package com.example.shoppinglist.service;

import com.example.shoppinglist.model.off.Product;
import com.example.shoppinglist.model.off.ProductList;
import com.example.shoppinglist.model.selection.Quantity;
import com.example.shoppinglist.model.shoppingList.Proposal;
import com.example.shoppinglist.utils.CandidateChooser;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class ProductPicker {

  private CandidateChooser candidateChooser;
  private final KeycloakRestTemplate keycloakRestTemplate;

  public ProductPicker(CandidateChooser candidateChooser, KeycloakRestTemplate keycloakRestTemplate){
    this.candidateChooser=candidateChooser;
    this.keycloakRestTemplate = keycloakRestTemplate;
  }
  public List<Proposal> pick(List<Quantity> baseList, String store, int i) throws IOException {
    List<Proposal> mapIngredientToProduct = new ArrayList<>();
    for (Quantity quantity:baseList) {
      List<Product> candidates = getCandidates(quantity,store,i);
      Proposal proposal = new Proposal();
      proposal.setQuantity(quantity);
      if(candidates.size()!=0){
        proposal.setProduct(candidates.get(0));
      }
      mapIngredientToProduct.add(proposal);
    }
    return mapIngredientToProduct;
}

  public List<Product> getCandidates(Quantity quantity,String store, int size) {

    final String uri = "http://localhost:8084/products/{ingredientKey}/{ingredientName}/{number}/{unit}/{store}/?size={size}";
    Map<String, Object> params = createParams(quantity, store,size);
    ResponseEntity<ProductList> response =  keycloakRestTemplate.exchange(uri,HttpMethod.GET,null, ProductList.class,params);
    ProductList candidateList = response.getBody();
    if(candidateList.products != null){
      return candidateList.products;
    }
    else{
      return new ArrayList<>();
    }
  }

  private Map<String, Object> createParams(Quantity quantity,String store,int size) {
    String ingredientKey = quantity.getIngredient().getKey();
    String ingredientName = quantity.getIngredient().getName();
    String unit = quantity.getUnit().getKey();
    int number = quantity.getQuantity();
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("ingredientKey",ingredientKey);
    params.put("ingredientName",ingredientName);
    params.put("unit",unit);
    params.put("number",number);
    params.put("store",store);
    params.put("size",size);
    return params;
  }
}
